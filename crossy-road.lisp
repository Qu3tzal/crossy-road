(clear-all)
(define-model crossy-road-model   
	; (sgp :esc t :v nil :lf .05 :ul t :trace-detail medium)
	(sgp :v nil :esc t :egs 3 :show-focus nil :trace-detail medium :ult t)
	(sgp-fct (list :ul t))
	; (sgp :v t :esc t :lf 0.4 :bll 0.5 :ans 0.5 :rt 0 :ncnar nil)

	(load "ACT-R:CrossyRoad;interface.lisp")
	(load "ACT-R:CrossyRoad;goals.lisp")
	(load "ACT-R:CrossyRoad;declarative-knowledge.lisp")
	(load "ACT-R:CrossyRoad;init-dm.lisp")
	(load "ACT-R:CrossyRoad;productions.lisp")
)
