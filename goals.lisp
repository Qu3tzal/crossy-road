;; Goal and sub-goals chunk types.
(chunk-type goal state light-color cars-left cars-right)
(chunk-type goal-attend-light read-state)
(chunk-type goal-attend-car state position x)
(chunk-type check-outcome check-state)
(chunk-type do-cross go)

(define-chunks 
    (start) 
	;;State for the visual 
	(attending) (attending-light) (attending-car) 
	(respond) (handle) (evaluate-danger) (blocked-left) (blocked-right)
	(interpret)
	;;Final states
	(cross) (wait))
