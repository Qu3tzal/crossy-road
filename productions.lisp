;; Production rules.
; On cherche ou se trouve le feu de signalisation
(P find-traffic-light-location
	=goal>
		isa goal-attend-light
		read-state find-location
==>
	+visual-location>
		<= screen-y 60
	=goal>
		read-state attending
)

; On regarde le le feu de signalisation
(P attend-traffic-light-location
	=goal>
		isa goal-attend-light
		read-state attending
	=visual-location>
	?visual>
		state free
==>
	+visual>
		cmd         move-attention
        screen-pos  =visual-location
	=goal>
		read-state respond
)

; On voit la couleur du feu est on essaye de l'interpreter
(p respond-traffic-light-location
	=goal>
		isa goal-attend-light
		read-state respond
	=visual>
		color =color
==>
	+retrieval>
    	isa color-meaning
    	color =color
	=goal>
		read-state interpret
)

; On voit que la couleur du feu indique que l'on peut traverser en sécurité
(p respond-traffic-light-meaning-safe
	=goal>
		isa goal-attend-light
		read-state interpret
	=retrieval>
		isa color-meaning
		safe t
==>
	+goal>
		isa do-cross
		go t
)

; On voit que la couleur du feu indique que l'on ne peut pas traverser en sécurité
(p respond-traffic-light-meaning-unsafe
	=goal>
		isa goal-attend-light
		read-state interpret
	=retrieval>
		isa color-meaning
		safe nil
==>
	+goal>
		isa goal-attend-car
		state find-location
		position left
)

; On cherche une voiture arrivant par la gauche
(p find-car-left
	=goal>
		isa goal-attend-car
		state find-location
		position left
==>
	+visual-location>
		> screen-y 250
		< screen-y 350
		< screen-x 300
		> screen-x 0
		screen-x highest
	=goal>
		state attending
)

; On cherche une voiture arrivant par la droite
(p find-car-right
	=goal>
		isa goal-attend-car
		state find-location
		position right
==>
	+visual-location>
		> screen-y 100
		< screen-y 200
		> screen-x 300
		< screen-x 600
		screen-x lowest
	=goal>
		state attending
)

; On voit qu'il n'y a pas de voitures à gauche, on regarde à droite
(p attend-car-left-no-car
	=goal>
		isa goal-attend-car
		position left
		state attending
	?visual-location>
		buffer failure
==>
	=goal>
		position right
		state find-location
)

; On voit qu'il n'y à pas de voitures à droite, on peut traverser
(p attend-car-right-no-car
	=goal>
		isa goal-attend-car
		position right
		state attending
	?visual-location>
		buffer failure
==>
	+goal>
		isa do-cross
		go t
)

; On observe les détails de la voiture reperée
(p attend-car
	=goal>
		isa goal-attend-car
		state attending
	=visual-location>
		screen-x =x
	?visual>
		state free
==>
	+visual>
		cmd         move-attention
        screen-pos  =visual-location
	=goal>
		state respond
		x =x
)

; Procédure appellée en cas d'erreur pendant la lecture de la voiture, on recommance le cycle
(p respond-car-error
	=goal>
		isa goal-attend-car
		state respond
		position =direction
		x =distance
	?visual>
		error t
	; ?manual>
	; 	state free
==>
	; +manual>
		; cmd     press-key
		; key "b"
	-visual-location>
	-visual>
	+goal>
		isa goal-attend-light
		read-state find-location
)

; On stocke les données de la voiture observée dans le buffer imaginal
(p respond-car
	=goal>
		isa goal-attend-car
		state respond
		position =direction
		x =distance
	=visual>
		value =label
	?imaginal>
		state free
==>
	=goal>
		state handle
	+imaginal>
		isa car
		distance =distance
		direction  =direction
		label =label
)

; On évalue le risque présenté par la voiture présente dans le buffer imaginal
; Ici les distances sont suffisantes pour traverser en toute sécurité
(P eval-car-risk-left-go-cross-safe
	=goal>
		ISA 		goal-attend-car
		state		handle
	=imaginal>
		ISA car
		direction 	left
		label 		=label
		< distance 	150
==>
	+goal>
		state find-location
		position right
)

; On évalue le risque présenté par la voiture présente dans le buffer imaginal
; Ici on juge que la voiture est trop proche pour pouvoir traverser en sécurité
(P eval-car-risk-left-no-cross
	=goal>
		ISA 		goal-attend-car
		state		handle
	=imaginal>
		ISA car
		direction 	left
		label 		=label
		> distance 	150
==>
	+goal>
		state blocked-left
)


; On évalue le risque présenté par la voiture présente dans le buffer imaginal
; Ici les distances sont suffisantes pour traverser en toute sécurité
(P eval-car-risk-right-go-cross-safe
	=goal>
		ISA 		goal-attend-car
		state		handle
	=imaginal>
		ISA car
		direction 	right
		label 		=label
		> distance 	450
==>
	+goal>
		isa do-cross
		go t
)

; On évalue le risque présenté par la voiture présente dans le buffer imaginal
; Ici on juge que la voiture est trop proche pour pouvoir traverser en sécurité
(P eval-car-risk-right-no-cross
	=goal>
		ISA 		goal-attend-car
		state		handle
	=imaginal>
		ISA car
		direction 	right
		label 		=label
		< distance 	450
==>
	=goal>
		state blocked-right
)


; On évalue le risque présenté par la voiture présente dans le buffer imaginal
; Ici on évalue que l'on peut traverser en sécurité alors que les conditions 
; ne le permettent enfaite pas
(P eval-car-risk-right-go-cross-unsafe
	=goal>
		ISA 		goal-attend-car
		state		handle
	=imaginal>
		ISA car
		direction 	right
		label 		=label
		> distance 	400
		< distance  450
==>
	+goal>
		isa do-cross
		go t
)


; On évalue le risque présenté par la voiture présente dans le buffer imaginal
; Ici on évalue que l'on peut traverser en sécurité alors que les conditions 
; ne le permettent enfaite pas
(P eval-car-risk-left-go-cross-unsafe
	=goal>
		ISA 		goal-attend-car
		state		handle
	=imaginal>
		ISA car
		direction 	left
		label 		=label
		< distance 	200
		> distance 	150
==>
	+goal>
		state find-location
		position right
)



; Un char bloque la traversée à droite, on informe la simulation de ce blocage
(P press-block-right
	=goal>
		ISA 		goal-attend-car
		state blocked-right
	; ?manual>
	; 	state free
==>
	; +manual>
	; 	cmd     press-key
	; 	key "r"
	; -goal>
	-visual-location>
	-visual>
	+goal>
		isa goal-attend-light
		read-state find-location
)

; Un char bloque la traversée à gauche, on informe la simulation de ce blocage
(P press-block-left
	=goal>
		ISA 		goal-attend-car
		state blocked-left
	; ?manual>
	; 	state free
==>
	; +manual>
	; 	cmd     press-key
	; 	key "l"
	; -goal>
	-visual-location>
	-visual>
	+goal>
		isa goal-attend-light
		read-state find-location
)


; Le modèle à jugé que l'on pouvais traverser, on informe la simulation de cette décision
(P cross-yes
	=goal>
		isa do-cross
		go t
	?manual>
		state free
==>
	+manual>
		cmd     press-key
		key "c"
	+goal>
		isa check-outcome
		check-state find-location
	-visual-location>
	-visual>
)



#| Check l'outcome de la décision Good ou Bad |#
(P find-test-outcome
	=goal>
		isa check-outcome
		check-state find-location
==>
	+visual-location>
		> screen-y 200
		< screen-y 250
		> screen-x 275
		< screen-x 325
	=goal>
		check-state attending
)

(P attend-test-outcome
	=goal>
		isa check-outcome
		check-state attending
	=visual-location>
	?visual>
		state free
==>
	+visual>
		cmd         move-attention
        screen-pos  =visual-location
	=goal>
		check-state respond
)

; Ici on voit que l'outcome est positif
; Avec utility learning on obtient un reward positif pour la séquence de procédures
(p respond-test-outcome-good
	=goal>
		isa check-outcome
		check-state respond
	=visual>
		value "G"
	?manual>
		state free
==>
	+goal>
		isa goal-attend-light
		read-state find-location
	+manual>
		cmd     press-key
		key "v"
)

; Ici on voit que l'outcome est négatif
; Avec utility learning on obtient un reward négatif pour la séquence de procédures
(p respond-test-outcome-bad
	=goal>
		isa check-outcome
		check-state respond
	=visual>
		value "B"
	?manual>
		state free
==>
	+goal>
		isa goal-attend-light
		read-state find-location
	+manual>
		cmd     press-key
		key "v"
)

; De mauvaises données sont présentes dans le buffer, on recommence la lecture de l'outcome
(p respond-test-outcome-miss
	=goal>
		isa check-outcome
		check-state respond
	=visual>
		value "v"
==>
	=goal>
		check-state find-location
)

; La lécture à échoué, on recommence la lecture de l'outcome
(p respond-test-outcome-error
	=goal>
		isa check-outcome
		check-state respond
	?visual-location>
		state error
==>
	=goal>
		check-state find-location
)


; Définition des rewards attribués quand les procédures sont atteintes
(spp find-traffic-light-location :reward 0)
(spp respond-test-outcome-bad :reward -10)
(spp respond-test-outcome-good :reward 10)

(goal-focus goal)
