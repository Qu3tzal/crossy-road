;; Declarative memory initialization.
(add-dm
	(goal isa goal-attend-light read-state find-location) ; Main goal
	(goal-attend-car ISA goal-attend-car) ; Sub goal
	(green-meaning isa color-meaning color GREEN safe t) ; Initialization of knowledge about the traffic-light color signification
	(red-meaning isa color-meaning color RED safe nil)
)
(goal-focus goal)
