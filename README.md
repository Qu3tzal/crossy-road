# Crossy Road

## Hypothèses
- Il existe un unique passage piéton accessible
- Il existe un feu pour les piétons et un pour les voitures
- La conduite se fait à droite
- Le passage piéton croise une route
    - à une voie et à double sens
    - droite
    - infinie
    - sans intersections
- Les conditions visuelles sont propices
- Pas d’obstacles empêchant de voir les voitures arriver
- Les sens visuels fonctionnent correctement
- Les conditions climatiques sont bonnes

Pour lancer le modèle on lance la commande : 
(run (crossy-road (carNumber runNumber)))
- carNumber: le nombre de voitures qu’on souhaite voir apparaître dans l’interface graphique 
- runNumber: le nombre d’itérations qu’on veut que le modèle exécute

