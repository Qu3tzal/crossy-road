;; Declarative knowledge chunk types.
(chunk-type car distance direction label danger) ; Encode a car's data
(chunk-type evaluated-car label danger) ; Encode a car's danger evaluation
(chunk-type traffic-light pos-x pos-y color) ; Encode the traffic-light position and color
(chunk-type color-meaning color safe) ; Encode knowledge about the meaning of the color of the traffic-light
